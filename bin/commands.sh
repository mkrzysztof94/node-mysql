#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

END_PROMPT=0
PROMPT=0

ACTIONS=("Exit" \
    "Bootup docker environment" \
    "Remove docker containers" \
    "Download dependencies" \
    "Mysql-master bash open" \
    "Mysql-master status" \
    "Mysql-master logs" \
    "Mysql-slave bash open" \
    "Mysql-slave status" \
    "Mysql-slave logs" \
    "Node-gyp menu" \
)

GYP_ACTIONS=("Back" \
    "Configure" \
    "Build" \
    "Clean" \
    "Debug configure" \
    "Debug build" \
)

CONTAINERS=(
    "dev-mysql-master" \
    "dev-mysql-slave" \
)


ResetCol="\x1B[0m"
BrBlue="\x1B[94;1m"
BrWhite="\x1B[97;1m"

function logInfo
{
    printf "${BrWhite}[${ResetCol}${BrBlue}INFO${ResetCol}${BrWhite}]${ResetCol}  ${BrWhite}$1${ResetCol}\n"
}

function removeContainers
{

    for((i=0;i < ${#CONTAINERS[@]};i++));
    do
        if docker ps -a | grep -q "${CONTAINERS[$i]}";
        then
            echo "Remoing ${CONTAINERS[$i]}"
            docker rm -f "${CONTAINERS[$i]}"  >> $DIR/../tmp/command.log
        fi
    done

    logInfo "Containers removed !"
}

function buildImages
{
    docker build -t dev/native-node $DIR/../docker/images/node >> $DIR/../tmp/command.log
    docker build -t dev/native-percona $DIR/../docker/images/percona >> $DIR/../tmp/command.log
    logInfo "Images builded"
}

function clearLogs
{
    rm -fR $DIR/../tmp/log/ || exit
    mkdir $DIR/../tmp/log
    mkdir $DIR/../tmp/log/master
    mkdir $DIR/../tmp/log/slave
    touch $DIR/../tmp/log/master/replication-bin.log || exit
    touch $DIR/../tmp/log/master/mysqld-relay-bin.log || exit
    chmod 777 -R $DIR/../tmp/log/
    cd $DIR/../docker
    logInfo "Logs cleared"
}

function initializeReplication
{
    while [ ! -f "$DIR/../tmp/log/master/replication-bin.000004" ]
    do
        logInfo "Waiting for mysql-master bootup"
        sleep 2
    done
    logInfo "Mysql-master booted"

    docker exec --privileged dev-mysql-master bash -c "mysql -s -u root -p\$MYSQL_ROOT_PASSWORD < /shared/init-master-repl.sql" >> $DIR/../tmp/command.log
    sleep 1
    logInfo "Mysql-master replication started"
    
    docker exec --privileged dev-mysql-slave bash -c "mysql -s -u root -p\$MYSQL_ROOT_PASSWORD < /shared/init-slave.sql" >> $DIR/../tmp/command.log
    sleep 1
    logInfo "Mysql-slave attached"
    
    docker exec --privileged dev-mysql-master bash -c "mysql -s -u root -p\$MYSQL_ROOT_PASSWORD < /shared/init-master.sql" >> $DIR/../tmp/command.log
    sleep 1
    replicationWatch > $DIR/../tmp/log/binlogWatch.log &
    logInfo "Replication initialized !"    
}

function replicationWatch
{
    mysqlbinlog --user=root --password=root --read-from-remote-server --start-position=4 --host=mysql-master --verbose --stop-never --stop-never-slave-server-id=3 replication-bin.000004
}

function startContainers
{
    docker-compose -f $DIR/../docker/docker-compose.yml up -d  >> $DIR/../tmp/command.log    
    logInfo "Containers started"    
}

function bootupEnv
{
    buildImages
    clearLogs
    startContainers
    initializeReplication
}

function mysqlMasterStatus
{
    docker exec --privileged dev-mysql-slave bash -c "mysql -u root -p\$MYSQL_ROOT_PASSWORD < /shared/slave-status.sql"
}

function mysqlMasterLogTail
{
    docker logs dev-mysql-master --tail 50 -f
}

function mysqlMasterOpenBash
{
    docker exec -it dev-mysql-master bash
}

function mysqlSlaveStatus
{
    docker exec --privileged dev-mysql-slave bash -c "mysql -u root -p\$MYSQL_ROOT_PASSWORD < /shared/slave-status.sql"
}

function mysqlSlaveLogTail
{
    docker logs dev-mysql-slave --tail 50 -f
}

function mysqlSlaveOpenBash
{
    docker exec -it dev-mysql-slave bash
}

function mysqlRepl
{
    mysqlreplicate --master=root:root@172.10.0.2:3306 --rpl-user=slave:slave --slave=root:root@172.10.0.3:3306 \
    --master-log-file=replication-bin.000004 --master-log-pos=0 -vvv --test-db=testy
}

function downloadDependencies
{
    rm -fR "$DIR/../src/native/deps/*"
    git clone -b 5.6 https://github.com/mysql/mysql-server.git "$DIR/../src/native/deps/mysql-5.6"
    logInfo "Dependency download completed."
}



function gypClean
{
    logInfo "Node-gyp clean started"
    node-gyp -C "$DIR/../src/native/" clean
    logInfo "Node-gyp clean finished"
}

function gypConfigure
{
    logInfo "Node-gyp configure started"
    node-gyp -C "$DIR/../src/native/" configure
    logInfo "Node-gyp configure finished"
}

function gypBuild
{
    logInfo "Node-gyp build started"
    node-gyp -C "$DIR/../src/native/" build
    logInfo "Node-gyp build finished"
}

function gypDebugConfigure
{
    logInfo "Node-gyp debug configure started"
    node-gyp -C "$DIR/../src/native/" --debug configure
    logInfo "Node-gyp debug configure finished"
}

function gypDebugBuild
{
    logInfo "Node-gyp debug build started"
    node-gyp -C "$DIR/../src/native/" --debug build
    logInfo "Node-gyp debug build finished"
}

function gypPromptRead
{
    case "$1" in
        "1") gypConfigure ;;
        "2") gypBuild ;;
        "3") gypClean ;;
        "4") gypDebugConfigure ;;
        "5") gypDebugBuild ;;
        "0") PROMPT=0 ;;
        *) echo "Action not exists"
    esac
}

function gypPrompt
{
    if [ "$#" = 0 ];
    then
        printf "${BrWhite}Node-gyp actions:${ResetCol}\n"
        for((i=1;i < ${#GYP_ACTIONS[@]};i++));
        do
            mainPromptPrintRow $i "${GYP_ACTIONS[$i]}"
        done
        mainPromptPrintRow 0 ${GYP_ACTIONS[0]}
        printf "${BrWhite}Select:${ResetCol}\n"
        read selection
        gypPromptRead $selection
    else
        gypPromptRead $1
    fi
}


function mainPromptPrintRow
{
    printf "  ${BrBlue}[${ResetCol}${BrWhite}${1}${ResetCol}${BrBlue}]${ResetCol} ${BrWhite}$2${ResetCol}\n"
}

function mainPromptPrint
{
    printf "${BrWhite}Actions:${ResetCol}\n"
    for((i=1;i < ${#ACTIONS[@]};i++));
    do
        mainPromptPrintRow $i "${ACTIONS[$i]}"
    done
    mainPromptPrintRow 0 ${ACTIONS[0]}
    printf "${BrWhite}Select:${ResetCol}\n"
}

function mainPromptRead
{
    case "$1" in
        "1") bootupEnv ;;
        "2") removeContainers ;;
        "3") downloadDependencies ;;
        "4") mysqlMasterOpenBash ;;
        "5") mysqlMasterStatus ;;
        "6") mysqlMasterLogTail ;;
        "7") mysqlSlaveOpenBash ;;
        "8") mysqlSlaveStatus ;;
        "9") mysqlSlaveLogTail ;;
        "10") PROMPT=1 ;;
        "0") END_PROMPT=1 ;;
        *) echo "Action not exists"
    esac
}

function mainPrompt
{
    if [ "$#" = 0 ]; then
        while [ $END_PROMPT == 0 ]
        do
            case "$PROMPT" in
                "0") mainPromptPrint && read selection && mainPromptRead $selection ;; 
                "1") gypPrompt ;; 
            esac
            sleep 1
        done
    elif [ "$#" = 2 ] && [ "$1" = 10 ]; then
        gypPrompt $2
    else 
        mainPromptRead $1
    fi
    
}


mainPrompt $@