#include "bootstrap.h"

namespace Native
{

    uv_buf_t* Utils::GetBuffer(std::string str)
    {
        uv_buf_t* buf = new uv_buf_t(uv_buf_init((char*) malloc(str.size()), str.size()));
        buf->len      = str.copy(buf->base, str.size());
        return buf;
    }

    void Utils::ErrorPrinter(const char* errorPrefix, int errorStatus)
    {
        if (errorStatus < 0)
            fprintf(stderr, std::string(errorPrefix).append(": %s\n").c_str(), uv_strerror(errorStatus));
    }

    const char* Utils::v8StrToCharPtr(Local<String> str)
    {
        return str->GetExternalOneByteStringResource()->data();
    }

    const char* Utils::ToChar(const Local<Value>& val)
    {
        return *(*new String::Utf8Value(val)) ?: "<string conversion failed>";
    }

    const char* Utils::ToChar(const Local<String>& val)
    {
        return *(*new String::Utf8Value(val)) ?: "<string conversion failed>";
    }

    std::string Utils::ToStr(Local<Value> val)
    {
        return std::string(Utils::ToChar(val));
    }

    void Utils::writeStringToStream(uv_stream_t* dest, std::string str, uv_write_cb cb)
    {
        int len = (int) str.length();

        uv_buf_t    buffer = uv_buf_init((char*) malloc(len + 1), (unsigned int) len);
        uv_write_t* req    = (uv_write_t*) malloc(sizeof(uv_write_t));

        memcpy(buffer.base, str.c_str(), len);
        Utils::ErrorPrinter("Tty write error: ", uv_write(req, dest, &buffer, (unsigned int) 1, cb));
    }

    void Utils::writeBufToStream(uv_stream_t* dest, size_t size, uv_buf_t buf, uv_write_cb cb)
    {
        write_req_t* req = (write_req_t*) malloc(sizeof(write_req_t));
        req->buf         = uv_buf_init((char*) malloc(size), size);
        Utils::ErrorPrinter("Tty write error: ", uv_write((uv_write_t*) req, (uv_stream_t*) dest, &buf, 1, cb));
    }

    void Utils::freeBufAfterWrite(uv_write_t* req, int status)
    {
        if (status == -1)
            fprintf(stderr, "freeBufAfterWrite error: %s", uv_err_name(status));
        else
            free(req);
    }

    void Utils::allocBuffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf)
    {
        *buf = uv_buf_init((char*) malloc(suggested_size), suggested_size);
    }

    const char* Utils::ToCString(const String::Utf8Value& value)
    {
        return *value ? *value : "<string conversion failed>";
    }

    void Utils::PrintArray(Local<v8::Array> arr)
    {

        std::cout << std::endl;

        for (int i = 0; i < (int) arr->Length(); i++)
            std::cout << Utils::ToStr(arr->Get(i)).append("\n").c_str() << std::endl;

        std::cout << std::endl;
    }

    void Utils::PrintArray(std::vector<std::string> arr)
    {

        std::cout << std::endl;

        for (std::string& row : arr)
            std::cout << row.append("\n").c_str() << std::endl;

        std::cout << std::endl;
    }

    Local<v8::Array> Utils::FindInArray(Local<v8::Array> arr, std::string searchFor)
    {
        Local<v8::Array> result = v8::Array::New(arr->GetIsolate());
        std::string      tmpc   = std::string();

        for (int i = 0; i < (int) arr->Length(); i++)
        {
            tmpc = Utils::ToStr(arr->Get(i));
            if ((int) tmpc.find(searchFor) >= 0)
                result->Set(result->Length(), arr->Get(i));
        }

        return result;
    }

    void Utils::ThrowErrorException(const char* msg, Isolate* iso)
    {
        iso->ThrowException(v8::Exception::Error(String::NewFromUtf8(iso, msg)));
    }
}