#include "bootstrap.h"

namespace Native
{

    JSClassBuilder* JSClassBuilder::Create(const char* className, FunctionCallback constructorCallback)
    {
        return (new JSClassBuilder(className, constructorCallback));
    }
    JSClassBuilder::JSClassBuilder(const char* className, FunctionCallback constructorCallback)
    {
        this->className          = className;
        this->constructorCb      = constructorCallback;
        this->prototypeFunctions = new NameFunctionMap();
        this->staticFunctions    = new NameFunctionMap();
    }

    JSClassBuilder::~JSClassBuilder()
    {
    }

    JSClassBuilder* JSClassBuilder::AddFunction(NameFunctionPair protoPair)
    {
        this->prototypeFunctions->insert(protoPair);
        return this;
    }
    JSClassBuilder* JSClassBuilder::AddFunction(const char* funcName, FunctionCallback cb)
    {
        this->AddFunction(NameFunctionPair(std::string(funcName), cb));
        return this;
    }
    JSClassBuilder* JSClassBuilder::AddFunction(NameFunctionPair* protoPairArr)
    {
        for (uint i = 0; i < sizeof(protoPairArr); i++)
        {
            this->prototypeFunctions->insert(protoPairArr[i]);
        }
        return this;
    }

    JSClassBuilder* JSClassBuilder::AddStaticFunction(NameFunctionPair staticPair)
    {
        this->staticFunctions->insert(staticPair);
        return this;
    }
    JSClassBuilder* JSClassBuilder::AddStaticFunction(const char* funcName, FunctionCallback cb)
    {
        this->AddFunction(NameFunctionPair(std::string(funcName), cb));
        return this;
    }
    JSClassBuilder* JSClassBuilder::AddStaticFunction(NameFunctionPair* staticPairArr)
    {
        for (uint i = 0; i < sizeof(staticPairArr); i++)
        {
            this->staticFunctions->insert(staticPairArr[i]);
        }
        return this;
    }

    Local<ObjectTemplate> JSClassBuilder::BuildPrototype(Local<ObjectTemplate> jsProto, Isolate* isolate)
    {
        for (const NameFunctionPair& pair : *this->prototypeFunctions)
        {
            jsProto->Set(isolate, (char*) pair.first.c_str(), FunctionTemplate::New(isolate, pair.second));
        }
        return jsProto;
    }
    Local<FunctionTemplate> JSClassBuilder::BuildStatic(Local<FunctionTemplate> jsClass, Isolate* isolate)
    {
        for (const NameFunctionPair& pair : *this->staticFunctions)
        {
            jsClass->Set(isolate, (char*) pair.first.c_str(), FunctionTemplate::New(isolate, pair.second));
        }
        return jsClass;
    }

    Local<FunctionTemplate> JSClassBuilder::BuildClass(Isolate* isolate, bool destructAfterBuild)
    {
        Local<FunctionTemplate> con = FunctionTemplate::New(isolate, this->constructorCb);
        con->InstanceTemplate()->SetInternalFieldCount(1);
        con->SetClassName(String::NewFromUtf8(isolate, this->className));
        this->BuildPrototype(con->PrototypeTemplate(), isolate);

        if (destructAfterBuild)
            delete this;

        return this->BuildStatic(con, isolate);
    }
}