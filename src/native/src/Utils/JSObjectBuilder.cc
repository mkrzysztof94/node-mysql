
#include "bootstrap.h"

namespace Native
{

    JSObjectBuilder* JSObjectBuilder::Create()
    {
        return (new JSObjectBuilder());
    }

    JSObjectBuilder::JSObjectBuilder()
    {
        this->functions  = new NameFunctionMap();
        this->properties = new NamePropertyMap();
    }

    JSObjectBuilder* JSObjectBuilder::AddFunction(NameFunctionPair protoPair)
    {
        this->functions->insert(protoPair);
        return this;
    }

    JSObjectBuilder* JSObjectBuilder::AddFunction(const char* funcName, FunctionCallback cb)
    {
        this->AddFunction(NameFunctionPair(std::string(funcName), cb));
        return this;
    }

    JSObjectBuilder* JSObjectBuilder::AddFunction(NameFunctionPair* protoPairArr)
    {
        for (uint i = 0; i < sizeof(protoPairArr); i++)
        {
            this->functions->insert(protoPairArr[i]);
        }
        return this;
    }

    JSObjectBuilder* JSObjectBuilder::AddProperty(const char* propertyName, Local<Object> property)
    {
        this->properties->insert(NamePropertyPair(std::string(propertyName), property));
        return this;
    }

    JSObjectBuilder::~JSObjectBuilder()
    {
    }

    Local<Object> JSObjectBuilder::BuildObject(Isolate* isolate, bool destructAfterBuild)
    {
        Local<Object> obj = this->BuildProperties(this->BuildFunctions(Object::New(isolate), isolate), isolate);

        if (destructAfterBuild)
            delete this;

        return obj;
    }

    Local<Object> JSObjectBuilder::BuildObject(Local<Object> target, Isolate* isolate, bool destructAfterBuild)
    {
        Local<Object> obj = this->BuildProperties(this->BuildFunctions(target, isolate), isolate);

        if (destructAfterBuild)
            delete this;

        return obj;
    }

    Local<Object> JSObjectBuilder::BuildProperties(Local<Object> target, Isolate* isolate)
    {
        for (const NamePropertyPair& pair : *this->properties)
        {
            target->Set(
                isolate->GetCurrentContext(), String::NewFromUtf8(isolate, (char*) pair.first.c_str()), pair.second);
        }
        return target;
    }

    Local<Object> JSObjectBuilder::BuildFunctions(Local<Object> target, Isolate* isolate)
    {
        for (const NameFunctionPair& pair : *this->functions)
        {
            target->Set(
                isolate->GetCurrentContext(),
                String::NewFromUtf8(isolate, (char*) pair.first.c_str()),
                FunctionTemplate::New(isolate, pair.second)->GetFunction());
        }
        return target;
    }
}