#include "bootstrap.h"

namespace Native
{
    using namespace node;
    using namespace v8;

    void MainModule::Initialize(Local<Object> exports, Local<Value> module, Local<Context> context)
    {
        Isolate*    iso = context->GetIsolate();
        HandleScope Scope(iso);
    }
}

NODE_MODULE_CONTEXT_AWARE(NODE_GYP_MODULE_NAME, Native::MainModule::Initialize);