{
    "targets": [
        {
            "target_name": "Native",
            "include_dirs" : [
                "include"
            ],
            "defines":[
                "NODE_WANT_INTERNALS=1",
                "NODE_SHARED_MODE=1"
            ],
            "sources": [
                "src/MainModule.cc",
                "src/Utils/*.cc"
            ],
            
          "cflags": [

          ],
          "cflags_cc": [

          ]
        }
    ]
}
