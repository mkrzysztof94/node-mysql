#ifndef __NATIVE_UTILS_TYPES_H__
#define __NATIVE_UTILS_TYPES_H__

namespace Native
{
    struct write_req_s
    {
        uv_write_t req;
        uv_buf_t   buf;
    };

    typedef const v8::FunctionCallbackInfo<v8::Value>& FunctionCallbackArg;
    typedef std::map<std::string, v8::FunctionCallback>   NameFunctionMap;
    typedef std::pair<std::string, v8::FunctionCallback>  NameFunctionPair;
    typedef std::map<std::string, v8::Local<v8::Object>>  NamePropertyMap;
    typedef std::pair<std::string, v8::Local<v8::Object>> NamePropertyPair;
    typedef struct write_req_s write_req_t;
}

#endif