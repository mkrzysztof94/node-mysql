#ifndef __NATIVE_UTILS_H__
#define __NATIVE_UTILS_H__

#include "Utils/Types.h"

namespace Native
{

    using namespace node;
    using namespace v8;

    class Utils
    {
      public:
        static uv_buf_t* GetBuffer(std::string str);
        static void ErrorPrinter(const char* errorPrefix, int errorStatus);
        static const char* v8StrToCharPtr(v8::Local<v8::String> str);
        static const char* ToChar(const v8::Local<v8::Value>& val);
        static const char* ToChar(const v8::Local<v8::String>& val);
        static std::string ToStr(v8::Local<v8::Value> val);
        static void writeStringToStream(uv_stream_t* dest, std::string str, uv_write_cb cb = Utils::freeBufAfterWrite);
        static void writeBufToStream(
            uv_stream_t* dest, size_t size, uv_buf_t buf, uv_write_cb cb = Utils::freeBufAfterWrite);

        static void freeBufAfterWrite(uv_write_t* req, int status);
        static void allocBuffer(uv_handle_t* handle, size_t suggested_size, uv_buf_t* buf);
        static const char* ToCString(const v8::String::Utf8Value& value);
        static void PrintArray(Local<v8::Array> arr);
        static void PrintArray(std::vector<std::string> arr);
        static Local<v8::Array> FindInArray(Local<v8::Array> arr, std::string searchFor);
        static void ThrowErrorException(const char* msg, Isolate* iso);
    };

    class JSClassBuilder
    {
      public:
        static JSClassBuilder* Create(const char* className, FunctionCallback constructorCallback);
        JSClassBuilder(const char* className, FunctionCallback constructorCallback);
        ~JSClassBuilder();
        JSClassBuilder* AddFunction(NameFunctionPair protoPair);
        JSClassBuilder* AddFunction(const char* funcName, FunctionCallback cb);
        JSClassBuilder* AddFunction(NameFunctionPair* protoPairArr);
        JSClassBuilder* AddStaticFunction(NameFunctionPair staticPair);
        JSClassBuilder* AddStaticFunction(const char* funcName, FunctionCallback cb);
        JSClassBuilder* AddStaticFunction(NameFunctionPair* staticPairArr);
        Local<FunctionTemplate> BuildClass(Isolate* isolate, bool destructAfterBuild = true);

      protected:
        Local<ObjectTemplate> BuildPrototype(Local<ObjectTemplate> jsProto, Isolate* isolate);
        Local<FunctionTemplate> BuildStatic(Local<FunctionTemplate> jsClass, Isolate* isolate);
        NameFunctionMap* prototypeFunctions;
        NameFunctionMap* staticFunctions;
        const char*      className;
        FunctionCallback constructorCb;
    };

    class JSObjectBuilder
    {
      public:
        static JSObjectBuilder* Create();
        JSObjectBuilder();
        ~JSObjectBuilder();
        JSObjectBuilder* AddFunction(NameFunctionPair protoPair);
        JSObjectBuilder* AddFunction(const char* funcName, FunctionCallback cb);
        JSObjectBuilder* AddFunction(NameFunctionPair* protoPairArr);
        JSObjectBuilder* AddProperty(const char* propertyName, Local<Object> property);
        Local<Object> BuildObject(Isolate* isolate, bool destructAfterBuild = true);
        Local<Object> BuildObject(Local<Object> target, Isolate* isolate, bool destructAfterBuild = true);

      protected:
        Local<Object> BuildProperties(Local<Object> target, Isolate* isolate);
        Local<Object> BuildFunctions(Local<Object> target, Isolate* isolate);
        NameFunctionMap* functions;
        NamePropertyMap* properties;
    };
}

#endif
