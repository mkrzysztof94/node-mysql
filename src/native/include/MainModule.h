#ifndef __NATIVE_MAIN_MODULE_H__
#define __NATIVE_MAIN_MODULE_H__

namespace Native
{
    class MainModule
    {
      public:
        static void Initialize(
            v8::Local<v8::Object> exports, v8::Local<v8::Value> module, v8::Local<v8::Context> context);
    };
}

#endif