#ifndef __NATIVE_BOOTSTRAP_H__
#define __NATIVE_BOOTSTRAP_H__

#define NODE_WANT_INTERNALS 1
#define NODE_SHARED_MODE 1
#define USING_V8_SHARED 1

#include "stddef.h"
#include "stdlib.h"
#include "stdint.h"
#include "stdio.h"
#include "iostream"
#include "string.h"
#include "bits/allocator.h"
#include "memory.h"
#include "map"

#include "v8.h"
#include "v8-util.h"
#include "v8config.h"
#include "node.h"
#include "uv.h"

namespace Native
{
    class MainModule;
    class Utils;
    class JSClassBuilder;
    class JSObjectBuilder;

    struct write_req_s;
}

#include "MainModule.h"
#include "Utils/Utils.h"

#endif