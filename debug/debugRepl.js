'use strict'
const native = require('../src/native/build/Debug/Native.node')
const Repl = require('repl')

let con = {
    native: native
}

let repl = Repl.start({ prompt: '==>' })

repl.context = Object.assign(repl.context, con)
